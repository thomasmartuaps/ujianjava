<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Place Order</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Ujian Java</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/UjianJava">Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <ul class="navbar-nav my-2 my-lg-0">
      <li class="nav-item active">
      <c:if test="${sessionScope.account eq null}">
<a class="nav-link" href='<c:url value="/login"></c:url>'>Login</a>
</c:if>
      <c:if test="${sessionScope.account ne null}">
<a class="nav-link" href='<c:url value="/logout"></c:url>'>Logout<span class="sr-only">(current)</span></a>
</c:if>
      </li>
    </ul>
  </div>
</nav>
	<div class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
		<div>
			<br>
			<h1>
				Order
				<c:out value="${account.username}"></c:out>
			</h1>
			<br>
			<p>
				<a href="/UjianJava/item-form">Add a product</a>
			</p>

			<%-- <c:if test="${sessionScope.account eq null}"> --%>
			<%--   <p><a href='<c:url value="/login"></c:url>'>Login</a></p> --%>
			<%-- </c:if> --%>
			<br>
			<c:if test="${order.orderItem.size() > 0}">
			
			<table width="80%" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th scope="col">Code</th>
						<th scope="col">Name</th>
						<th scope="col">Price</th>
						<th scope="col">Qty</th>
						<th scope="col">Total Price</th>
						<th scope="col">Actions</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${order.orderItem}" var="item">
						<tr>
							<td scope="col"><c:out value="${item.product.code}"></c:out></td>
							<td scope="col"><c:out value="${item.product.name}"></c:out></td>
							<td scope="col"><c:out value="${item.product.price}"></c:out></td>
							<td scope="col"><c:out value="${item.quantity}"></c:out></td>
							<td scope="col"><c:out
									value="${item.quantity * item.product.price}"></c:out></td>
							<td scope="col"><a href="confirm-delete/${item.sessionId}">Delete</a></td>
						</tr>

					</c:forEach>
				</tbody>
			</table>

			<form:form method="post" action="order">
				<input class="btn btn-success" type="submit" value="Save" />
			</form:form>
</c:if>
<c:if test="${order.orderItem eq null || order.orderItem.size() == 0}">
<p>
Please add items to cart
			</p>
			</c:if>
		</div>
		<br>
		<%-- <c:if test="${sessionScope.account ne null}"> --%>
		<%--   <p><a href='<c:url value="/logout"></c:url>'>Logout</a></p> --%>
		<%-- </c:if> --%>
	</div>
</body>
</html>