<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Orders List</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Ujian Java</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/UjianJava">Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <ul class="navbar-nav my-2 my-lg-0">
      <li class="nav-item active">
      <c:if test="${sessionScope.account eq null}">
<a class="nav-link" href='<c:url value="/login"></c:url>'>Login</a>
</c:if>
      <c:if test="${sessionScope.account ne null}">
<a class="nav-link" href='<c:url value="/logout"></c:url>'>Logout<span class="sr-only">(current)</span></a>
</c:if>
      </li>
    </ul>
  </div>
</nav>
<div class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
		<div>
			<br>
			<h1>
				All Orders
				<c:out value="${account.username}"></c:out>
			</h1>
			<br>
			<p>
				<a href="/UjianJava/checkout">Create new order</a>
			</p>

			<br>

			
			<table width="80%" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th scope="col" width="15%">#</th>
						<th scope="col" width="25%">createdAt</th>
						<th scope="col">Items</th>
						<th scope="col">Total Qty</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${orders}" var="order">
						<tr>
							<td scope="col"><c:out value="${order.id}"></c:out></td>
							<td scope="col"><c:out value="${order.createdAt}"></c:out></td>
							<td scope="col">
							<c:forEach items="${order.orderItem}" var="item">
								<c:out value="${item.product.name}"></c:out>, 
							</c:forEach>
							</td>
							<td scope="col"><c:out value="${order.totalAmount()}"></c:out></td>
						</tr>

					</c:forEach>
				</tbody>
			</table>



	</div>
</body>
</html>