<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Confirm Delete</title>
<style>
.submit {color: blue;}
.submit:hover {text-decoration: underline; cursor: pointer;}
</style>
</head>
<body>
<h1>Are you sure you want to remove this from cart?</h1>
<br>
<form:form id="deleteItem" action="/UjianJava/remove-item" method="POST">
<form:input path="sessionId" type="hidden" value="${sessionId}"/>
<div class="submit" onClick="submitDelete">Delete</div>
<br>
</form:form>
<a href="/UjianJava/checkout">Back</a>

<script type="text/javascript">
	function submitDelete() {
		console.log("hey")
		console.log(${sessionId})
		const form = document.getElementById("deleteItem")
		form.submit();
	}
	document.querySelector(".submit").addEventListener("click", function () {
		console.log("hey")
		const form = document.getElementById("deleteItem")
		form.submit();
	});
</script>
</body>
</html>