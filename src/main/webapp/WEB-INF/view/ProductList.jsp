
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Product List</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Ujian Java</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/UjianJava">Home <span class="sr-only">(current)</span></a></li>
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">
				<li class="nav-item active"><c:if
						test="${sessionScope.account eq null}">
						<a class="nav-link" href='<c:url value="/login"></c:url>'>Login</a>
					</c:if> <c:if test="${sessionScope.account ne null}">
						<a class="nav-link" href='<c:url value="/logout"></c:url>'>Logout<span
							class="sr-only">(current)</span></a>
					</c:if></li>
			</ul>
		</div>
	</nav>
	<div class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
		<h1 class="display-4">Product List</h1>

		<div>
			<a href="/UjianJava/">Back to Index</a> <br> <a
				href="/UjianJava/product/new">Create new product</a>
		</div>

		<table width="80%" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th scope="col">Code</th>
					<th scope="col">Name</th>
					<th scope="col">Type</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${products}" var="product">
					<tr>
						<td><c:out value="${product.code}"></c:out></td>
						<td><c:out value="${product.name}"></c:out></td>
						<td><c:out value="${product.type}"></c:out></td>
						<td><a href="product/edit/${product.id}">View</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>