<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<div class="col-9 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" style="width: 35%">
<div class="card">
  <div class="card-body">
<c:form method="post" modelAttribute="account" class="form-group" action="login">
	<table>
		<tr>
			<td>Username</td>
			<td><c:input path="username" class="form-control"></c:input></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><c:password path="password" class="form-control"></c:password></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>		
		</td>
	</table>
			<td><input type="submit" class="btn btn-success" value="login"></input></c:form>
  </div>
</div>
</div>

</body>
</html>