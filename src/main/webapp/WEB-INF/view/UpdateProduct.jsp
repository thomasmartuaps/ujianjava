<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Item</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Ujian Java</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/UjianJava">Home <span class="sr-only">(current)</span></a></li>
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">
				<li class="nav-item active"><c:if
						test="${sessionScope.account eq null}">
						<a class="nav-link" href='<c:url value="/login"></c:url>'>Login</a>
					</c:if> <c:if test="${sessionScope.account ne null}">
						<a class="nav-link" href='<c:url value="/logout"></c:url>'>Logout<span
							class="sr-only">(current)</span></a>
					</c:if></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<h1 class="display-4">Update Product</h1>
		</div>
		<div class="row">
			<form:form method="post" action="/UjianJava/product/update">
				<h5>Product Name:</h5>
				<br>
				<form:input path="name" type="text" />
				<br>
				<h5>Product Code:</h5>
				<br>
				<form:input path="code" type="text" />
				<br>
				<h5>Product Type:</h5>
				<br>
				<form:input path="type" type="text" />
				<br>
				<h5>Price:</h5>
				<br>
				<form:input path="price" type="number" />
				<br>
				<br>
				<form:input path="id" type="hidden" />
				<input type="submit" value="Save" />
			</form:form>
		</div>
	</div>
</body>
</html>