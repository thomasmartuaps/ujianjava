<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Failed!</title>
</head>
<body>
<h1>Failed to <c:out value="${fail_type}"></c:out> product!</h1>
<c:out value="${message}"></c:out>
<br>
<a href='/product/new'>Back to form</a>
</body>
</html>