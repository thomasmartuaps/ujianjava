<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>New Product</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Ujian Java</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="/UjianJava">Home <span class="sr-only">(current)</span></a></li>
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">
				<li class="nav-item active"><c:if
						test="${sessionScope.account eq null}">
						<a class="nav-link" href='<c:url value="/login"></c:url>'>Login</a>
					</c:if> <c:if test="${sessionScope.account ne null}">
						<a class="nav-link" href='<c:url value="/logout"></c:url>'>Logout<span
							class="sr-only">(current)</span></a>
					</c:if></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<h1 class="display-4">New Product</h1>
		</div>
		<div class="row">
			<form:form method="post" action="create">
				<div class="form-group">
					<label for="name">Product Name</label>

					<form:input path="name" type="text" />
				</div>
				<div class="form-group">
					<label for="code">Product Code</label>
					<form:input path="code" type="text" />
				</div>

				<div class="form-group">
					<label for="type">Product Type</label>
					<form:input path="type" type="text" placeholder="e.g. drinks" />

				</div>

				<div class="form-group">
					<label for="price">Price</label>
					<form:input path="price" type="number" />
				</div>


<!-- 				<div class="form-group"> -->
<!-- 					<label for="stock">Stock</label> -->
<%-- 					<form:input path="stock" type="number" /> --%>
<!-- 				</div> -->

				<input class="btn btn-success" type="submit" value="Save" />
			</form:form>
		</div>
	</div>
</body>
</html>