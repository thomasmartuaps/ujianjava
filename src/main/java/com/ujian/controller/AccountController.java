package com.ujian.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.ujian.domain.Account;
import com.ujian.services.AccountService;



@Controller
public class AccountController {
	@Autowired
	private AccountService service;

	@GetMapping("/login")
	public String loginDisplay(Model theModel, HttpSession session) {
		Account account = new Account();
		theModel.addAttribute("account", account);
		return "login";
	}

	@PostMapping("/login")
	public String login(@ModelAttribute("account") Account account, Model theModel, HttpSession session) {
		account = service.loginAccount(account);
		System.out.println("account" + account);
		if (account != null) {
			theModel.addAttribute("account", account);
			session.setAttribute("account", account);
			return "redirect:" + "checkout";
		}
		if (account == null) {
			theModel.addAttribute("message", "Invalid Credentials");
		}
		return "loginfailed";
	}

	@GetMapping("/logout")
	public String logoutDisplay(Model theModel, HttpSession session) {
		if (session.getAttribute("account") != null) {
			session.invalidate();
			theModel.addAttribute("message", "You have logout Successfully!!!");
		}
		return "logout";
	}
}
