package com.ujian.controller;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.ujian.domain.Order;
import com.ujian.domain.Product;
import com.ujian.services.OrderService;
import com.ujian.services.ProductService;


@Controller
public class ExportController {
	@Autowired
	ProductService proServ;

	@GetMapping("/export")
	public void exportToCSV(HttpServletResponse response) {
		response.setContentType("text/csv");
		String fileName = "order.csv";

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=" + fileName;

		response.setHeader(headerKey, headerValue);
		try {
			List<Object> object = proServ.showAll();
			ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
			String[] csvHeader = {"id_order","quantity","code","name","type","price"};
			String[] nameMapping = {"idFk","quantity","product"};

			csvWriter.writeHeader(csvHeader);

			for (Object objectList: object) {
				System.out.println(objectList);
				csvWriter.write(objectList, nameMapping);
			}

			csvWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
