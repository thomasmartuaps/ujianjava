package com.ujian.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.ujian.domain.Account;
import com.ujian.domain.ConfirmDeleteForm;
import com.ujian.domain.Order;
import com.ujian.domain.OrderItem;
import com.ujian.services.OrderService;

@Controller
public class OrderController {

	@Autowired
	private OrderService orderService;

	@GetMapping("/")
	public String indexPage(Model m, HttpSession session) {
		if (session.getAttribute("account") == null) {
			return "redirect:" + "login";
		}

		return "Index";
	}

	@GetMapping("/checkout")
	public String showCheckoutPage(@SessionAttribute(name = "order", required = false) Order order, Model m, HttpSession session) {
		if (session.getAttribute("account") == null) {
			return "redirect:" + "login";
		}
		if (order != null) {
			m.addAttribute("order", order);
		} else {
			m.addAttribute("order", new Order());

		}
		return "CheckoutPage";
	}


	@GetMapping("/confirm-delete/{id}")
	public String confirmDeletePage(@PathVariable("id") String id, Model m) {
		m.addAttribute("sessionId", id);
		m.addAttribute("command", new ConfirmDeleteForm());
		return "ConfirmDelete";
	}

	@PostMapping("/order")
	public String submitOrder(@SessionAttribute(name = "order") Order order, Account account, Model m,
			HttpSession session) {
		if (session.getAttribute("account") == null) {
			return "redirect:" + "login";
		} else {
			if (order != null) {
				List<OrderItem> items = order.getOrderItem();
				if (items != null) {
					try {
						orderService.save(order);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						m.addAttribute("message", "Class not found exception");
						return "FailedOrder";
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						m.addAttribute("message", "IO exception");
						return "FailedOrder";
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						m.addAttribute("message", "SQL Error");
						return "FailedOrder";
					}
				} else {
					m.addAttribute("message", "No items added to the order");
					return "FailedOrder";
				}
			} else {
				m.addAttribute("message", "No order, try again.");
				return "FailedOrder";
			}
			m.addAttribute("message", "Success creating order!");
			return "redirect:" + "success";
		}
	}
	
	@GetMapping("/orders")
	public String ordersPage(Model m, HttpSession session) {
		if (session.getAttribute("account") == null) {
			return "redirect:" + "login";
		}
		List<Order> orders;
		try {
			orders = orderService.findAll();
			m.addAttribute("orders", orders);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "OrdersList";
	}

}
