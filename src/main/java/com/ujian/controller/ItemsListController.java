package com.ujian.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ujian.domain.ConfirmDeleteForm;
import com.ujian.domain.Order;
import com.ujian.domain.OrderItem;
import com.ujian.domain.OrderItemForm;
import com.ujian.domain.Product;
import com.ujian.services.ProductService;

@Controller
@SessionAttributes("order")
public class ItemsListController {

	@Autowired
	private ProductService productService;

	@PostMapping("/save")
	public String addToCart(@ModelAttribute Order order, @ModelAttribute("orderItem") OrderItemForm orderItemForm, Model m) {
		OrderItem orderItem = new OrderItem();

		/* copy-pasted logic to generate sessionId */
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int)
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    String sessionId = buffer.toString();
	    
	
	    orderItem.setSessionId(sessionId);
		orderItem.setQuantity(orderItemForm.getQuantity());
		Product product;
		try {
			product = productService.findById(orderItemForm.getProduct_id());
			orderItem.setProduct(product);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (order != null) {
			System.out.println("old order on save");
//			add to items list
			List<OrderItem> items = order.getOrderItem();
			if (items != null) {
				System.out.println("items exist");
				items.add(orderItem);

			} else {
				items = new ArrayList<>();
				items.add(orderItem);
			}
			order.setOrderItem(items);
			m.addAttribute("order", order);
		} else {
			System.out.println("new order on save");
			List<OrderItem> items = new ArrayList<>();
			items.add(orderItem);
			Order newOrder = new Order();
			newOrder.setOrderItem(items);
			m.addAttribute("order", newOrder);
		}
		return "redirect:" + "checkout";
	}

	@PostMapping("/remove-item")
	public String removeFromCart(@ModelAttribute Order order, @ModelAttribute("confirmDelete") ConfirmDeleteForm confirmDelete, Model m) {
		List<OrderItem> items = order.getOrderItem();
		System.out.println(items);
		System.out.println("before removed");
		System.out.println(confirmDelete.getSessionId());
		System.out.println("remove this one");
		items.removeIf(i -> i.getSessionId().equals(confirmDelete.getSessionId()));
		System.out.println(items);
		order.setOrderItem(items);
		m.addAttribute("order", order);
//		System.out.println(confirmDelete.getSessionId());

		return "redirect:" + "checkout";
	}

	
	@GetMapping("/success")
	public String cartSuccess(@ModelAttribute Order order, Model m) {
		m.addAttribute("order", new Order());
		m.addAttribute("message", "Success creating order!");
		
		return "FinishOrder";
	}
	
	@GetMapping("/item-form")
	public String showAddItemPage(Model m) {
		try {
			List<Product> products = productService.findAll();
			System.out.println("dapat produk");
			System.out.println(products);
			m.addAttribute("products", products);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m.addAttribute("command", new OrderItemForm());
		return "AddItem";
	}

	@ModelAttribute("order")
	public Order order() {
		return new Order();
	}
}
