package com.ujian.controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.ujian.domain.Product;
import com.ujian.services.ProductService;

@Controller
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/product")
	public String showProductList(Model m) {
//		ProductService productService = new ProductService();
		try {
			List<Product> products = productService.findAll();
			m.addAttribute("products", products);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ProductList";
	}

	@GetMapping("/product/new")
	public String showNewProductForm(Model m) {
		m.addAttribute("command", new Product());
		return "CreateProduct";
	}

	@GetMapping("/product/edit/{id}")
	public String showUpdateProductForm(@PathVariable("id") int id, Model m) {
		try {
			@SuppressWarnings("unchecked")
			Product product = productService.findById(id);
			m.addAttribute("command", product);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "UpdateProduct";
	}

	@PostMapping("/product/create")
	public String create(@ModelAttribute("product") Product newProduct, Model m) {
		System.out.println("where is the new product");
		System.out.println(newProduct);
		System.out.println("this is the new product");
		try {
			productService.save(newProduct);
			m.addAttribute("success_type", "added!");
			return "SuccessCreate";
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "create");
			m.addAttribute("message", "Class Not Found Exception");
			return "FailedCreate";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "create");
			m.addAttribute("message", "SQL Error!");
			return "FailedCreate";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FailedCreate";
		}
	}

	@PostMapping("/product/update")
	public String update(@ModelAttribute("product") Product product, Model m) {
		System.out.println(product);
		try {
			productService.updateProduct(product);
			m.addAttribute("success_type", "updated!");
			return "SuccessCreate";
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "update");
			m.addAttribute("message", "Class Not Found Exception");
			return "FailedCreate";
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "update");
			m.addAttribute("message", "IO Error!");
			return "FailedCreate";
		}
	}
}
