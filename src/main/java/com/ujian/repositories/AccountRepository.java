package com.ujian.repositories;

import com.ujian.domain.Account;

public interface AccountRepository {

	public Account loginAccount(Account account);
}
