package com.ujian.repositories;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.ujian.domain.OrderItem;

public interface OrderItemRepository {

	List<OrderItem> findAll() throws IOException, ClassNotFoundException, SQLException;

	void save(OrderItem orderitem) throws IOException, ClassNotFoundException, SQLException;
}
