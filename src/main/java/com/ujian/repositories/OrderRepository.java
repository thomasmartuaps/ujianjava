package com.ujian.repositories;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.ujian.domain.Order;
import com.ujian.domain.Product;

public interface OrderRepository {
	List<Order> findAll() throws IOException, ClassNotFoundException, SQLException;

	Order findById(int id) throws IOException, ClassNotFoundException, SQLException;

	void save(Order order) throws IOException, ClassNotFoundException, SQLException;

	void save(Product product) throws IOException, ClassNotFoundException, SQLException;

}

