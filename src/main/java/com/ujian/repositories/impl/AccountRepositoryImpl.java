package com.ujian.repositories.impl;


import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ujian.domain.Account;
import com.ujian.repositories.AccountRepository;

@Repository
@Transactional
public class AccountRepositoryImpl implements AccountRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Account loginAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		try {
		  Query<Account> query = session.createQuery("from Account where username =:username and password =:password",Account.class);
		  query.setParameter("username", account.getUsername());
		  query.setParameter("password", account.getPassword());
		  account = query.getSingleResult();
		  return account;
		} catch (NoResultException e) {
		   // TODO: handle exception
		return null;
		}
	}

}
