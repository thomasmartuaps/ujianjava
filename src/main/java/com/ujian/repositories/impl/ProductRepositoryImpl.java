package com.ujian.repositories.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ujian.domain.Order;
import com.ujian.domain.OrderItem;
import com.ujian.domain.Product;
import com.ujian.repositories.ProductRepository;

@Repository
public class ProductRepositoryImpl implements ProductRepository {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Product> findAll() throws IOException, ClassNotFoundException, SQLException {
		System.out.println("find all products");
		Session session = sessionFactory.getCurrentSession();
		Query<Product> product = session.createQuery("from Product", Product.class);
		System.out.println(product.list());
		List<Product> productList = product.list();
		return productList;
	}
	
	@Override
	public List<Object> showAll(){
		Session session = sessionFactory.getCurrentSession();
		Query product = session.createQuery("from OrderItem", OrderItem.class);
		List<Object> productList = product.list();
		return productList;
	}

	@Override
	public Product findById(int id) throws IOException, ClassNotFoundException, SQLException {
		Session session = sessionFactory.getCurrentSession();
		Product product = session.get(Product.class, id);
		return product;
	}

	@Override
	public void save(Product product) throws IOException, ClassNotFoundException, SQLException {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		System.out.println(product);
		System.out.println("should insert to db");
		session.persist(product);
		session.getTransaction().commit();
		session.close();

	}

	@Override
	public void save(OrderItem orderitem) throws IOException, ClassNotFoundException, SQLException {
		Session session = sessionFactory.getCurrentSession();
		session.persist(orderitem);

	}

	@Override
	public void updateProduct(Product product) throws IOException, ClassNotFoundException {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(product);
	}
}
