package com.ujian.repositories.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ujian.domain.OrderItem;
import com.ujian.repositories.OrderItemRepository;

@Repository
public class OrderItemRepositoryImpl implements OrderItemRepository {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<OrderItem> findAll() throws IOException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(OrderItem orderItem) throws IOException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		session.persist(orderItem);
		session.close();
	}

}
