package com.ujian.repositories.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ujian.domain.Order;
import com.ujian.domain.Product;
import com.ujian.repositories.OrderRepository;

@Repository
public class OrderRepositoryImpl implements OrderRepository {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Order> findAll() throws IOException, ClassNotFoundException, SQLException {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Query<Order> order = session.createQuery("from Order", Order.class);
		List<Order> orders = order.list();
		System.out.println(orders);
		System.out.println("hi buddy");
		session.close();
		return orders;
	}

	@Override
	public Order findById(int id) throws IOException, ClassNotFoundException, SQLException {
		Session session = sessionFactory.getCurrentSession();
		Order order = session.get(Order.class, id);
		return order;
	}

	@Override
	public void save(Order order) throws IOException, ClassNotFoundException, SQLException {
		Session session = sessionFactory.getCurrentSession();
		session.persist(order);
	}

	@Override
	public void save(Product product) throws IOException, ClassNotFoundException, SQLException {
		Session session = sessionFactory.getCurrentSession();
		session.persist(product);
	}

}
