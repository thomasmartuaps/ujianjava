package com.ujian.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "orderitem_tbl")
@SecondaryTable(name = "product_tbl")
public class OrderItem {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id")
	private int id;

	@Column(name = "id_fk1")
	private int idFk;

	@Column(name = "quantity")
	private int quantity;

	@OneToOne(cascade = CascadeType.MERGE)
	private Product product;

	private String sessionId;
	
	@Column(table = "product_tbl")
	private String code;

	@Column(table = "product_tbl")
	private String name;

	@Column(table = "product_tbl")
	private String type;
	
	@Column(table = "product_tbl")
	private Double price;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getIdFk() {
		return idFk;
	}

	public void setIdFk(int idFk) {
		this.idFk = idFk;
	}

	@Override
	public String toString() {
		return "OrderItem data: " + id + " - quantity: " + quantity + " - product: " + product.getName();
	}

	public OrderItem(int id, int quantity, Product product, String sessionId) {
		this.id = id;
		this.quantity = quantity;
		this.product = product;
		this.sessionId = sessionId;
	}

	public OrderItem(){

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	

}
