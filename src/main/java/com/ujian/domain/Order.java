package com.ujian.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "order_tbl")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id_order")
	private int id;

	@Column(name = "createdat")
	private Date createdAt;

    @OneToMany(cascade={CascadeType.ALL})
    @JoinColumn(name="id_fk1")
	List<OrderItem> orderItem;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public List<OrderItem> getOrderItem() {
		return orderItem;
	}


	public void setOrderItem(List<OrderItem> orderItem) {
		this.orderItem = orderItem;
	}

	public Order(int id, Date createdAt, List<OrderItem> orderItem) {
		this.id = id;
		this.createdAt = createdAt;
		this.orderItem = orderItem;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", createdAt=" + createdAt + ", orderItem={" + orderItem.toString() + "}]";
	}
	
	public int totalAmount() {
		int total = 0;
		for (OrderItem item: orderItem) {
			total = total + item.getQuantity();
		}
		
		return total;
	}

	public Order() {

	}

}
