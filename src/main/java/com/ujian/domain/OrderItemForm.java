package com.ujian.domain;

public class OrderItemForm {
	private int id;

	private int quantity;

	private int product_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public OrderItemForm(int id, int quantity, int product_id) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.product_id = product_id;
	}

	public OrderItemForm() {

	}
}
