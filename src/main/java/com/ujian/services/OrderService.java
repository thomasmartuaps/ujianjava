package com.ujian.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.ujian.domain.Order;

public interface OrderService {
	List<Order> findAll() throws IOException, ClassNotFoundException, SQLException;

	Order findById(int id) throws IOException, ClassNotFoundException, SQLException;

	void save(Order order) throws IOException, ClassNotFoundException, SQLException;

}