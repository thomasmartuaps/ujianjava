package com.ujian.services.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ujian.domain.OrderItem;
import com.ujian.domain.Product;
import com.ujian.repositories.ProductRepository;
import com.ujian.services.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository prodRepo;

	@Override
	@Transactional
	public List<Product> findAll() throws IOException, ClassNotFoundException, SQLException {
		List<Product> product = null;
		product = prodRepo.findAll();
		return product;
	}
	
	@Override
	@Transactional
	public List<Object> showAll() {
		List<Object> product = null;
		product = prodRepo.showAll();
		return product;
	}


	@Override
	@Transactional
	public Product findById(int id) throws IOException, ClassNotFoundException, SQLException {
		Product product = null;
		product = prodRepo.findById(id);
		return product;
	}

	@Override
	@Transactional
	public void save(Product product) throws IOException, ClassNotFoundException {

		try {

			prodRepo.save(product);
		} catch (ClassNotFoundException | IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	@Transactional
	public void save(OrderItem orderitem) throws IOException, ClassNotFoundException {

		try {

			prodRepo.save(orderitem);
		} catch (ClassNotFoundException | IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	@Transactional
	public void updateProduct(Product product) throws IOException, ClassNotFoundException {
		try {
			prodRepo.updateProduct(product);
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
