package com.ujian.services.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ujian.domain.Account;
import com.ujian.repositories.AccountRepository;
import com.ujian.services.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accRepo;

	@Override
	@Transactional
	public Account loginAccount(Account account) {
		return accRepo.loginAccount(account);
	}


}
