package com.ujian.services.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ujian.domain.Order;
import com.ujian.repositories.OrderRepository;
import com.ujian.services.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Override
	@Transactional
	public List<Order> findAll() throws IOException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		List<Order> orders = orderRepository.findAll();
		return orders;
	}

	@Override
	@Transactional
	public Order findById(int id) throws IOException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void save(Order order) throws IOException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		order.setCreatedAt(new Date());
		orderRepository.save(order);
//		for (OrderItem item: orderItems) {
//			System.out.println(item);
//			System.out.println("saving this item");
//			orderItemRepository.save(item);
//
//		}
	}

}
