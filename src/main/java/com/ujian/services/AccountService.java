package com.ujian.services;

import com.ujian.domain.Account;

public interface AccountService {
	public Account loginAccount(Account account);
}
