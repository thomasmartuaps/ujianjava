package com.ujian.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.ujian.domain.OrderItem;
import com.ujian.domain.Product;

public interface ProductService {
	List<Product> findAll() throws IOException, ClassNotFoundException, SQLException;

	List<Object> showAll();
	
	Product findById(int id) throws IOException, ClassNotFoundException, SQLException;

	void save(Product product) throws IOException, ClassNotFoundException, SQLException;

	void save(OrderItem orderitem) throws IOException, ClassNotFoundException, SQLException;

	void updateProduct(Product product) throws IOException, ClassNotFoundException;

}
